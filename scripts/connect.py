import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
import mysql
#import ytdl
from datetime import datetime
#def insertPythonVaribleInTable(link, name, description, duration, location, status):
def insertPythonVaribleInTable(video_id, video_url, url, video_title, video_description, video_duration, file_location, status):
    connection = mysql.connector.connect(host='localhost',
    database='grabit',
    user='website',
    password='P455w0rd',
    use_pure=True)
    try:
        cursor = connection.cursor(prepared=True)
        sql_insert_query = """ INSERT INTO `youtube`
                     (`video_id`, `video_url`, `url`, `video_title`, `video_description`, `video_duration`, `file_location`, `status`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"""
        insert_tuple = (video_id, video_url, url, video_title, video_description, video_duration, file_location, status)
        result  = cursor.execute(sql_insert_query, insert_tuple)
        connection.commit()
        #print ("Record inserted successfully into python_users table")
    except mysql.connector.Error as error :
        connection.rollback()
        print("Failed to insert into MySQL table {}".format(error))
    finally:
        #closing database connection.
        if(connection.is_connected()):
            cursor.close()
            connection.close()
            #print("MySQL connection is closed")
