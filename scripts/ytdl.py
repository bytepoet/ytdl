#!/usr/bin/python
from __future__ import unicode_literals
import youtube_dl
import sys
import json
import requests
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
import connect

url = str(sys.argv[1])
location = ''
status =''

class MyLogger(object):
    def debug(self, msg):
        pass
    def warning(self, msg):
        pass
    def error(self, msg):
        print(msg)



def console_hook(d):
    if d['status'] == 'finished':
        finalname = (d['filename'])

        #print('Done downloading, now converting ...')
        #connect.insertPythonVaribleInTable(url, "video_title", video_description, "video_filesize", "location", "status")

        if d['status'] == 'downloading':
            if d.get('eta') is not None:
                print(d['_percent_str'])
            else:
                print('Unknown ETA')

ydl_opts = {
        'format': 'best',
        'logger': MyLogger(),
        'progress_hooks': [console_hook],
        #'outtmpl': temp_filepath,
        'forcefilename': True,
        'forceurl': True,         #Force printing final URL.
        'forcetitle': True,       # Force printing title.
        'forceid': True,         #Force printing ID.
        'forcethumbnail': True,    #Force printing thumbnail URL.
        'forcedescription': True, #Force printing description.
        'forcefilename': True,    #Force printing final filename.
        'forceduration': True,    #Force printing duration.
        'forcejson': True,        #Force printing info_dict as JSON.
        'restrictfilenames': True,
        #'outtmpl': '/var/www/scripts/%(title)s-%(id)s.%(ext)s',
        'outtmpl': '/var/www/html/youtubedownloads/%(id)s.%(ext)s',
        }
with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    result1 = ydl.download([url])
    result2 = ydl.extract_info(url)
    #print result1
    print result2

video_id = result2.get(u'id')
video_url = result2.get(u'url')
video_download = result2.get(u'url')
video_title = result2.get(u'title')
video_description = result2.get(u'description')
video_filesize = result2.get(u'filesize')
video_duration = result2.get(u'duration')
#print(video_title)
#print(video_filesize)
#print(video_description)
#d_list = json.loads(result2)
#print(json.dumps(d_list))
connect.insertPythonVaribleInTable(video_id, video_url, url, video_title, video_description, video_duration, location, status)
#connect.insertPythonVaribleInTable(url, video_title, video_description, video_filesize, "location", "status")
