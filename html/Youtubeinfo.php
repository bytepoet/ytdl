<?php
$servername = "localhost";
$username = "website";
$password = "P455w0rd";
$dbname = "grabit";

$rx=shell_exec("/var/www/scripts/rx.sh 2>&1");
$tx=shell_exec("/var/www/scripts/tx.sh 2>&1");


function human_filesize($size, $precision = 2) {
    for($i = 0; ($size / 1024) > 0.9; $i++, $size /= 1024) {}
    return round($size, $precision).['B','kB','MB','GB','TB','PB','EB','ZB','YB'][$i];
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql1 = "SELECT * FROM youtube";
$result1 = $conn->query($sql1);

#$sql2 = "SELECT id, link, foldername, status FROM youtube  WHERE status = 'working'";
#$result2 = $conn->query($sql2);

#$sql3 = "SELECT id, link, foldername, status FROM youtube WHERE status = 'success' OR status = 'zipping'";
#$result3 = $conn->query($sql3);

#$sql4 = "SELECT id, link, foldername, status FROM youtube WHERE status = 'fail'";
#$result4 = $conn->query($sql4);

$conn->close();
?>

                               <div class="code">
                                TX: <?php print_r($tx); ?>MB<br>
                                RX: <?php print_r($rx); ?>MB
                               </div>


<?php

/* get disk space free (in bytes) */
$df = disk_free_space("/var/www");
/* and get disk space total (in bytes)  */
$dt = disk_total_space("/var/www");
/* now we calculate the disk space used (in bytes) */
$du = $dt - $df;
/* percentage of disk used - this will be used to also set the width % of the progress bar */
$dp = sprintf('%.2f',($du / $dt) * 100);

/* and we formate the size from bytes to MB, GB, etc. */
$df = formatSize($df);
$du = formatSize($du);
$dt = formatSize($dt);

function formatSize( $bytes )
{
        $types = array( 'B', 'KB', 'MB', 'GB', 'TB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $types ) -1 ); $bytes /= 1024, $i++ );
                return( round( $bytes, 2 ) . " " . $types[$i] );
}

?>
<style type='text/css'>

.progress {
        border: 2px solid #5E96E4;
        height: 32px;
        width: 540px;
        margin: 30px auto;
}
.progress .prgbar {
        background: #00FFFF;
        width: <?php echo $dp; ?>%;
        position: relative;
        height: 32px;
        z-index: 999;
}
.progress .prgtext {
        color: #ffaaff;
        text-align: center;
        font-size: 16px;
        padding: 0px 0 0;
        width: 540px;
        position: absolute;
        z-index: 1000;
}
.progress .prginfo {
        margin: 3px 0;
}

</style>


<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#000;}
.tg td{font-family:Arial, sans-serif;font-size:12px;padding:0px 2px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
.tg th{font-family:Terminal, sans-serif;font-size:15px;font-weight:normal;padding:2px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#333333;}
.tg .tg-o5vb{border-color:#fe0000;text-align:left;vertical-align:top}
.tg .tg-yzm2{background-color:#222222;color:#ffffff;text-align:center;vertical-align:center}
.tg .tg-0lax{background-color:#333333;color:#aaafff;text-align:center;vertical-align:center}
</style>
<!DOCTYPE HTML>


<html>
        <head>
                <title>Eventually by HTML5 UP</title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
                <link rel="stylesheet" href="assets/css/main.css" />
        </head>
        <body class="is-preload">
<br>
<div class='progress'>
        <div class='prgtext'><?php echo $dp; ?>% Disk Used</div>
        <div class='prgbar'></div>
        <div class='prginfo'>
                <span style='float: left;'><?php echo "$du of $dt used"; ?></span>
                <span style='float: right;'><?php echo "$df of $dt free"; ?></span>
                <span style='clear: both;'></span>
        </div>
</div>

<?php
    if ($result1->num_rows > 0) {
    // output data of each row

    echo '<h6> Videos Downloaded </h6>';
    echo '<table class="tg"><tr>';
    echo '<th class="tg-yzm2">ID</th>';
    echo '<th class="tg-yzm2">Video ID</th>';
    echo '<th class="tg-yzm2">Video URL</th>';
    echo '<th class="tg-yzm2">url</th>';
    echo '<th class="tg-yzm2">Video Title</th>';
    echo '<th class="tg-yzm2">Video Description</th>';
    echo '<th class="tg-yzm2">Video Duration</th>';
    echo '<th class="tg-yzm2">File Location</th>';
    echo '<th class="tg-yzm2">Status</th>';
    echo '</tr>';
    while($row1 = $result1->fetch_assoc()) {
    echo '<tr>';
    echo '<th class="tg-0lax">' . $row1 ["id"]. '</th>';
    echo '<th class="tg-0lax">' . $row1 ["video_id"]. '</th>';
    #echo '<th class="tg-0lax">' . $row1 ["video_url"]. '</th>';
    #echo '<th class="tg-0lax">' . $row1 ["url"]. '</th>';
    echo '<th glass="tg-0lax"><a href = "' . $row1 ["video_url"]. '"><input type="button" value="view" /></a></th>';
    echo '<th class="tg-0lax">' . $row1 ["video_title"]. '</th>';
    echo '<th class="tg-0lax">' . $row1 ["video_description"]. '</th>';
    echo '<th class="tg-0lax">' . $row1 ["video_duration"]. '</th>';
    echo '<th class="tg-0lax">' . $row1 ["file_location"]. '</th>';
    echo '<th class="tg-0lax">' . $row1 ["status"]. '</th>';

    echo '<th glass="tg-0lax"><a href = "youtubedownloads/' . $row1 ["video_id"]. '.mp4"><input type="button" value="Download" /></a></th>';
    //echo '<th><a href = "delete.php?id='$id'"><input type='button' value='Verwijder afspraak' name='verwijderen'/></a></th>';
    echo '</tr>';

    }
} else {
    echo "0 new records";
}
?>
</tr>
</table>




          <!-- Header -->

                <!-- Footer -->
                        <footer id="footer">
                                <ul class="icons">
                                </ul>
                                <ul class="copyright">
                                </ul>

                        </footer>
                                <div class="code">
                                <a href = "/"><input type="submit" value="Home" /> <a href = "/info.php"><input type="submit" value="Refresh" />
                                </div>
                <!-- Scripts -->
                        <script src="assets/js/main.js"></script>

        </body>
</html>
